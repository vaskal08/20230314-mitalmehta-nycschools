//
//  Extension.swift
// Extension
//
//  Created by Mital Mehta on 03/14/23.
//  Copyright © 2023 Mital Mehta. All rights reserved.
//

import UIKit

extension UIViewController {
    
    func displayAlert(message : String,onDismissBack:Bool = false,root:Bool = false){
        let alert = UIAlertController(title: AlertText.kAppName, message: message, preferredStyle: .alert)
        let ok = UIAlertAction(title: AlertText.kOkButton, style: .default, handler: {_ in
            if onDismissBack{
                if root{
                    self.navigationController?.popToRootViewController(animated: true)
                }else{
                    self.navigationController?.popViewController(animated: true)}
            }
        })
        alert.addAction(ok)
        DispatchQueue.main.async {
            self.present(alert, animated: true)
        }
    }
}
