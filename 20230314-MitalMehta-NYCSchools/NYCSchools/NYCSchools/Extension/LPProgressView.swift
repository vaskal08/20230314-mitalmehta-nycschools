//
//  LPProgressView.swift
//  LPProgressView
//
//  Created by Mital Mehta on 03/14/23.
//  Copyright © 2023 Mital Mehta. All rights reserved.
//

import UIKit

open class NSProgressView {
    
    var containerView = UIView()
    var progressView = UIView()
    
    var activityIndicator = NVActivityIndicatorView(frame: CGRect(x: 10, y: 10, width: 40, height: 40), type: .ballScaleRippleMultiple)
    
    open class var shared: NSProgressView {
        struct Static {
            
            static let instance: NSProgressView = NSProgressView()
        }
        return Static.instance
    }
    
    open func showProgressView(_ view: UIView) {
        containerView.frame = view.frame
        containerView.center = view.center
        containerView.backgroundColor = UIColor(hex: 0xffffff, alpha: 0.3)
        
        progressView.frame = CGRect(x: 0, y: 0, width: 60, height: 60)
        progressView.center = view.center
        progressView.backgroundColor = UIColor(hex: 0x444444, alpha: 0.7)
        progressView.clipsToBounds = true
        progressView.layer.cornerRadius = 10
        
        activityIndicator.frame = CGRect(x: 10, y: 10, width: 40, height: 40)
        activityIndicator.color = UIColor.white
        progressView.addSubview(activityIndicator)
        progressView.backgroundColor = ProgressbarColor.PrimaryColor
        containerView.addSubview(progressView)
        view.addSubview(containerView)
        
        activityIndicator.startAnimating()
        
    }
    
    open func hideProgressView() {
        activityIndicator.stopAnimating()
        DispatchQueue.main.async {
            self.containerView.removeFromSuperview()
        }
    }
}

extension UIColor {
    
    convenience init(hex: UInt32, alpha: CGFloat) {
        let red = CGFloat((hex & 0xFF0000) >> 16)/256.0
        let green = CGFloat((hex & 0xFF00) >> 8)/256.0
        let blue = CGFloat(hex & 0xFF)/256.0
        
        self.init(red: red, green: green, blue: blue, alpha: alpha)
    }
}


