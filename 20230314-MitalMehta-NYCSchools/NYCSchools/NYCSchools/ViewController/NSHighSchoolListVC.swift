//
//  NSHighSchoolListVC.swift
//  NYCSchools
//
//  Created by Mital Mehta on 03/14/23.
//  Copyright © 2023 Mital Mehta. All rights reserved.
//

import UIKit

class NSHighSchoolListVC: UIViewController {
    
    // MARK:- All IBOutlet and property
    @IBOutlet weak var viewNavigation : UIView!
    @IBOutlet weak var tblView: UITableView!
    var schoolListModel : [NSSchoolListModel] = []
    let schoolListViewModel = NSSchoolListViewModel()
    
    // MARK:- View Controller Cycle
    override func viewDidLoad() {
        self.setUpVC()
        super.viewDidLoad()
        self.callWebService()
    }
    
    // MARK:- Setup View
    func setUpVC(){
        self.tblView.register(UINib(nibName: CellIdentifier.kListTvCell, bundle: nil), forCellReuseIdentifier: CellIdentifier.kListTvCell)
    }
    
    // MARK:- Web Service Call
    func callWebService(){
        NSProgressView.shared.showProgressView(view)
        schoolListViewModel.schoolListApi(fromVC:self) { [weak self] (resultInfo) in
            NSProgressView.shared.hideProgressView()
            switch resultInfo {
            case .success(let resultInfo):
                self?.schoolListModel = resultInfo
                DispatchQueue.main.async(execute: {
                    self?.tblView.reloadData()
                })
            case .failure( _):
                self?.displayAlert(message: ApiError.kApiError)
            }
        }
    }
}

// MARK:- UITableView Delegate and Data Source
extension NSHighSchoolListVC: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.schoolListModel.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier.kListTvCell, for: indexPath) as? NSListTvCell
        guard let cell = cell else { return UITableViewCell() }
        cell.configureCell(item: schoolListModel[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let schoolDetailsVC = StoryboardName.kMainSB.instantiateViewController(withIdentifier: ControllerName.kNSSchoolDetailsVC) as? NSSchoolDetailsVC
        guard let schoolDetailsVC = schoolDetailsVC else { return }
        schoolDetailsVC.schoolListModel    = schoolListModel[indexPath.row]
        self.navigationController?.pushViewController(schoolDetailsVC, animated: true)
    }
}
