//
//  NSSchoolDetailsVC.swift
//  NYCSchools
//
//  Created by Mital Mehta on 03/14/23.
//  Copyright © 2023 Mital Mehta. All rights reserved.
//

import UIKit

class NSSchoolDetailsVC: UIViewController {
    
    // MARK:- All IBOutlet and property
    @IBOutlet weak var viewNavigation: UIView!
    @IBOutlet weak var viewSub: UIView!
    @IBOutlet weak var lblTaskers: UILabel!
    @IBOutlet weak var lblMath: UILabel!
    @IBOutlet weak var lblWriting: UILabel!
    @IBOutlet weak var lblReading: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblNoData: UILabel!
    
    let schoolDetailViewModel = NSSchoolDetailViewModel()
    var schoolListModel : NSSchoolListModel?
    var schoolInfoDetailModel : NSSchoolDetailsModel?
    var schoolDetailsModel : [NSSchoolDetailsModel] = []
    
    // MARK:- View Controller Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewSub.isHidden   = true
        self.lblNoData.isHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.callWebService()
    }
    
    // MARK:- Back Button Action
    @IBAction func btnBackMainAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK:- Web Service Call
    func callWebService(){
        NSProgressView.shared.showProgressView(view)
        schoolDetailViewModel.schoolDetailtApi(fromVC: self, completion: { [weak self] (resultInfo) in
            NSProgressView.shared.hideProgressView()
            switch resultInfo {
            case .success(let resultInfo):
                let resultDisplayInfo = resultInfo.filter { $0.dbn == self?.schoolListModel?.dbn}
                self?.displayData(resultDisplayInfo: resultDisplayInfo)
            case .failure( _):
                self?.displayAlert(message: ApiError.kApiError)
            }
        })
    }
    
    fileprivate func resultCheckCount(resultDisplayInfo : [NSSchoolDetailsModel]?) -> Bool{
        if (resultDisplayInfo?.count ?? 0) > 0{
            lblNoData.isHidden = true
            viewSub.isHidden   = false
            return true
        }else{
            lblNoData.isHidden = false
            viewSub.isHidden   = true
            return false
        }
    }
    
    func displayData(resultDisplayInfo : [NSSchoolDetailsModel]?){
        DispatchQueue.main.async {
            if self.resultCheckCount(resultDisplayInfo: resultDisplayInfo) {
                self.schoolInfoDetailModel = resultDisplayInfo?[0]
                
                self.lblTitle.text      = (self.schoolInfoDetailModel?.school_name ?? "")
                self.lblTaskers.text    = (self.schoolInfoDetailModel?.num_of_sat_test_takers ?? "")
                self.lblMath.text       = (self.schoolInfoDetailModel?.sat_math_avg_score ?? "")
                self.lblWriting.text    = (self.schoolInfoDetailModel?.sat_writing_avg_score ?? "")
                self.lblReading.text    = (self.schoolInfoDetailModel?.sat_critical_reading_avg_score ?? "")
            }
        }
    }
}

