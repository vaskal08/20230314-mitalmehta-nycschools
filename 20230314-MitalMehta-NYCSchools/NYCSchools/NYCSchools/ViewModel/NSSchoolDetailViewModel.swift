//
//  NSSchoolDetailViewModel.swift
//  NYCSchools
//
//  Created by Mital Mehta on 03/14/23.
//  Copyright © 2023 Mital Mehta. All rights reserved.


import UIKit
import Foundation

class NSSchoolDetailViewModel {
    let schoolListApiManager = NSSchoolApiManager()
    
    func schoolDetailtApi(fromVC:UIViewController,  completion: @escaping (Result<[NSSchoolDetailsModel], Error>) -> ()) {
        schoolListApiManager.schoolDetailsManager(fromVC: fromVC) { (result)  in
            switch result {
            case .success(let detailsModel):
                completion(.success(detailsModel))
            case .failure(let err):
                completion(.failure(err))
            }
        }
    }
}
