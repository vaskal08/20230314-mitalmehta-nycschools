//
//  NSSchoolListViewModel.swift
//  NYCSchools
//
//  Created by Mital Mehta on 03/14/23.
//  Copyright © 2023 Mital Mehta. All rights reserved.

import UIKit
import Foundation

class NSSchoolListViewModel {
    let schoolListApiManager = NSSchoolApiManager()
    
    func schoolListApi(fromVC:UIViewController,  completion: @escaping (Result<[NSSchoolListModel], Error>) -> ()) {
        schoolListApiManager.schoolListApiManager(fromVC: fromVC) { (result)  in
            switch result {
            case .success(let schoolList):
                completion(.success(schoolList))
            case .failure(let err):
                completion(.failure(err))
            }
        }
    }
}
