//
//  NSDetailsTests.swift
//  NYCSchoolsTests
//
//  Created by Mital Mehta on 03/14/23.
//  Copyright © 2023 Mital Mehta. All rights reserved.
//


import XCTest
import Foundation

@testable import NYCSchools

class NSSchoolDetailsTests: XCTestCase {
    var controller : NSSchoolDetailsVC!
    
    override func setUp() {
        super.setUp()
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        controller = storyboard.instantiateViewController(withIdentifier: "NSSchoolDetailsVC") as? NSSchoolDetailsVC
        let _ = controller.view
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testViewDidLoad(){
        XCTAssertNotNil(controller.viewDidLoad())
    }
    
    func testViewWillAppear(){
        XCTAssertNotNil(controller.viewWillAppear(true))
    }
    
    func testbtnBackMainAction(){
        XCTAssertNotNil(controller.btnBackMainAction(UIButton()))
    }
    
    func testDisplayData(){
        XCTAssertNotNil(controller.displayData(resultDisplayInfo: [NSSchoolDetailsModel]()))
    }
    
    func testSchoolDetailsMockJsonMethods(){
        let bundle = Bundle(for: type(of: self))
        guard let fileUrl = bundle.url(forResource: "NYCDetailsTestsJson", withExtension: "json") else { return }
        guard let data = try? Data(contentsOf: fileUrl) else { return }
        do {
            let schoolDetailsModel = try JSONDecoder().decode(NSSchoolDetailsModel.self, from: data)
            XCTAssertNotNil(schoolDetailsModel)
        } catch {
            print("Model Mapping failed with error: \(error)")
        }
    }
}
