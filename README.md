Project : NYC Schools (Code Challenges)

App Name : NYC Schools

General Information
    •    Technology/Tool : Xcode 14.0.1 
    •    Language : Swift
    •    Support : All iPhone Device
    •    Orientation : Portrait
    •    MVVM Architecture
    •    Unit test cases
    •    
Why use MVVM Architecture?
    •    Code and responsibility will be divided into Model, View, ViewModel
    •    Reduce complexity
    •    Loosely coupled code
    •    Increase reusability
    •    Improve Testability and code coverage for the pass, fail, and exception use cases.
    •    More expressive - ViewModel better expresses the business logic for the view
    •    Code will be more agile, We can find issues and bugs in an earlier stage

Project Structure :
    •    View Controller Class
    •    ViewModel
    •    Model Class
    •    Storyboard
    •    Web-service Manager Class
    •    Constant File
    •    Unit Test Cases Class 
    •    Reachability 
    •    Progressbar
    •    Documentation Files (ReadMe, Screenshot)

Project :
    •    The project developed using native iOS Framework. 
    •    Added prefix “NS” (NYC Schools) for all classes.
    •    Test cases - Use mock json file for increase more code coverage.
